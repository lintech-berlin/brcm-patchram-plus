# brcm_patchram_plus

This is the Broadcom utility *brcm_patchram_plus*.

This program downloads a patchram files in the HCD format to Broadcom Bluetooth
based silicon and combo chips and and other utility functions.

## Build

```sh
make
```

## Install

```sh
make install
```

## Run

```sh
brcm_patchram_plus -d --patchram /lib/firmware/brcm/ --bd_addr $BD_ADDR --baudrate 3000000 --enable_hci --no2bytes --tosleep=2000 --enable_fork /dev/ttymxc2 &
```
