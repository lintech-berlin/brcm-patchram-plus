VERSION = 1.0

DESTDIR	?=
PREFIX 	?= usr

CFLAGS += -DVERSION=\"$(VERSION)\"

all: brcm_patchram_plus

brcm_patchram_plus: brcm_patchram_plus.c
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

install: brcm_patchram_plus
	install -m 755 $^ $(DESTDIR)/$(PREFIX)/bin

clean:
	rm -f brcm_patchram_plus
